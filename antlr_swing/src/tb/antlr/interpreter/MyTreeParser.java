package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	private GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer dodaj(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer odejmij(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer pomnoz(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer podziel(Integer a, Integer b) {
		return a / b;
	}
	
	protected void declareVar(String nazwa) {
		globalSymbols.newSymbol(nazwa);
	}
	
	protected Integer getVar(String nazwa) {
		return globalSymbols.getSymbol(nazwa);
	}
	
	protected Integer setVar(String nazwa, Integer wartosc) {
		globalSymbols.setSymbol(nazwa, wartosc);
		return wartosc;
	}
}
